angular.module( 'PRCS', [ 'ngRoute', 'ngAnimate', 'ngCookies', 'angular-loading-bar', 'PRCSControllers', 'ui.bootstrap' ] )
    .run( [ '$rootScope', '$route', function( $rootScope, $route ) {
        $rootScope.$on( '$routeChangeSuccess', function() {
            if ( typeof $route.current.title != "undefined" ) document.title = "TicketStack.co.uk" + $route.current.title;
        } );
    } ] )
    // Offset filter for paging lists
    .filter( 'offset', function() {
        return function( input, start ) {
            start = parseInt( start, 10 );
            if(typeof input !== "undefined") return input.slice( start );
        };
    } )
    .config( [ '$routeProvider',
        function( $routeProvider ) {
            $routeProvider.when( '/', {
                    templateUrl: 'views/home.html',
                    controller: 'HomePageController',
                    controllerAs: 'HomePageController',
                    title: ''
                } )
                .when( '/events/', {
                    templateUrl: 'views/events.html',
                    controller: 'EventsListController',
                    controllerAs: 'EventsListController',
                    title: ' - Events'
                } )
                .when( '/event/:id/', {
                    templateUrl: 'views/event.html',
                    controller: 'EventDetailForm',
                    controllerAs: 'EventDetailForm',
                    title: ' - Event Details'
                } )
                .when( '/event/:event_id/:run_id/:price_id', {
                    templateUrl: 'views/tickets.html',
                    controller: 'TicketSelectForm',
                    controllerAs: 'TicketSelectForm',
                    title: ' - Select Tickets'
                } )
                .when( '/event/:event_id/:run_id/:price_id/checkout/:quantity', {
                    templateUrl: 'views/checkout.html',
                    controller: 'CheckoutForm',
                    controllerAs: 'CheckoutForm',
                    title: ' - Checkout'
                } )
                .when( '/event/:event_id/:run_id/:price_id/guest_checkout/:quantity', {
                    templateUrl: 'views/guest.html',
                    controller: 'GuestCheckoutForm',
                    controllerAs: 'GuestCheckoutForm',
                    title: ' - Guest Checkout'
                } )
                .when( '/bookings', {
                    templateUrl: 'views/bookings.html',
                    controller: 'MyBookings',
                    controllerAs: 'MyBookings',
                    title: ' - My Bookings'
                } )
                .when( '/account', {
                    templateUrl: 'views/account.html',
                    controller: 'AccountDetails',
                    controllerAs: 'AccountDetails',
                    title: ' - My Account'
                } )
                .when( '/login', {
                    templateUrl: 'views/login.html',
                    controller: 'LoginForm',
                    controllerAs: 'LoginForm',
                    title: ' - Login'
                } )
                .when( '/register', {
                    templateUrl: 'views/register.html',
                    controller: 'RegisterForm',
                    controllerAs: 'RegisterForm',
                    title: ' - Register'
                } )
                .otherwise( {
                    redirectTo: '/events/'
                } );
        }
    ] );