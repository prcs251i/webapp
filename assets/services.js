angular.module( 'PRCSServices', [] )
    .constant( 'API_BASE_URL', 'http://xserve.uopnet.plymouth.ac.uk/modules/INTPROJ/PRCS251I/api' )
    .service( 'LoginService', [ '$http', 'API_BASE_URL', '$cookies', function( $http, API_BASE_URL, CookieManager ) {
        var s = this;
        s.customer = CookieManager.getObject( 'SavedCustomer' ) || null;
        s.customerLoginCheck = function( email, password ) {
            return $http.post( API_BASE_URL + '/auth/customerAuthRequest', {
                "email": email,
                "password": password
            } );
        }
        s.setCustomer = function( Customer ) {
            s.customer = Customer;
            CookieManager.putObject( 'SavedCustomer', Customer );
        }
        s.getCustomer = function() {
            return s.customer;
        }
        s.isLoggedIn = function() {
            return s.customer !== null;
        }
        s.getCustomerLive = function() {
            var id = s.getCustomer()
                .ID;
            return $http.get( API_BASE_URL + '/customers/' + id );
        }
        s.createCustomer = function( data ) {
            return $http.post( API_BASE_URL + '/customers/', data );
        }
        s.updateCustomer = function( id, data ) {
            return $http.put( API_BASE_URL + '/customers/' + id, data );
        }
    } ] )
    .factory( 'EventsService', [ '$http', 'API_BASE_URL', function( $http, API_BASE_URL ) {
        return {
            getTypes: function() {
                return $http.get( API_BASE_URL + '/event_types' );
            },
            getAll: function() {
                return $http.get( API_BASE_URL + '/events' );
            },
            get: function( id ) {
                return $http.get( API_BASE_URL + '/events/' + id );
            }
        };
    } ] )
    .factory( 'RunsService', [ '$http', 'API_BASE_URL', function( $http, API_BASE_URL ) {
        return {
            getAll: function() {
                return $http.get( API_BASE_URL + '/runs' );
            },
            get: function( id ) {
                return $http.get( API_BASE_URL + '/runs/' + id );
            }
        };
    } ] )
    .factory( 'PricesService', [ '$http', 'API_BASE_URL', function( $http, API_BASE_URL ) {
        return {
            getAll: function() {
                return $http.get( API_BASE_URL + '/prices' );
            },
            get: function( id ) {
                return $http.get( API_BASE_URL + '/prices/' + id );
            }
        };
    } ] )
    .factory( 'ReviewsService', [ '$http', 'API_BASE_URL', function( $http, API_BASE_URL ) {
        return {
            create: function( data ) {
                return $http.post( API_BASE_URL + '/reviews/', data );
            }
        };
    } ] )
    .service( 'BookingService', [ '$http', 'API_BASE_URL', function( $http, API_BASE_URL ) {
        var s = this;
        s.createBooking = function( data ) {
            return $http.post( API_BASE_URL + '/bookings/createBooking', data );
        }
        s.createGuestBooking = function( data ) {
            return $http.post( API_BASE_URL + '/bookings/createGuestBooking', data );
        }
        s.getCustomerBookings = function( customerID ) {
            return $http.get( API_BASE_URL + '/bookings/myBookings/' + customerID );
        }
    } ] )