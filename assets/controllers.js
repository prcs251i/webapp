function sortRuns( item ) {
    item.RUNS = item.RUNS.sort( function( a, b ) {
        var date_a = Date.parse( a.RUN_DATE );
        var date_b = Date.parse( b.RUN_DATE );
        if ( date_a < date_b ) return -1;
        else if ( date_a > date_b ) return 1;
        else return 0;
    } )
    if ( item.RUNS.length == 0 ) {
        item.START_DATE = null;
        item.END_DATE = null;
    } else {
        item.START_DATE = Date.parse( item.RUNS[ 0 ].RUN_DATE );
        item.END_DATE = Date.parse( item.RUNS[ item.RUNS.length - 1 ].RUN_DATE );
    }
    return item;
}

function sortReviews( item ) {
    item.REVIEWS = item.REVIEWS.sort( function( a, b ) {
            var date_a = Date.parse( a.REVIEW_DATE );
            var date_b = Date.parse( b.REVIEW_DATE );
            if ( date_a < date_b ) return -1;
            else if ( date_a > date_b ) return 1;
            else return 0;
        } )
        .reverse();
    return item;
}

function avgReviews( item ) {
    item.AVG_RATING = item.REVIEWS.reduce( function( sum, current ) {
        return sum + current.USER_RATING;
    }, 0 ) / item.REVIEWS.length;
    if ( item.REVIEWS.length == 0 ) item.AVG_RATING = 0;
    return item;
}
angular.module( 'PRCSControllers', [ 'PRCSServices' ] )
    .controller( 'NavController', [ '$route', 'LoginService', '$location', '$scope', function( $route, LoginService, $location, $scope ) {
        var vm = this;
        vm.is = function( current ) {
            if ( typeof $route.current !== "undefined" && typeof $route.current.loadedTemplateUrl !== "undefined" ) return ( current.match( $route.current.loadedTemplateUrl.replace( 'views/', '' ) ) ) ? "active" : "";
            else return "";
        }
        vm.isLoggedIn = function() {
            return LoginService.isLoggedIn();
        }
    } ] )
    .controller( 'AccountDetails', [ 'LoginService', '$location', function( LoginService, $location ) {
        var vm = this;
        vm.virtuals = {};
        var parsley = $( '#account-form' )
            .parsley();
        if ( !LoginService.isLoggedIn() ) $location.url( '/login?return_to=/account' );
        else LoginService.getCustomerLive()
            .then( function( res ) {
                res.data.EXPIRY_DATE = new Date( res.data.EXPIRY_DATE );
                vm.data = res.data;
                $( '#card-num' )
                    .formance( "format_credit_card_number" );
                $( '#card-cvc' )
                    .formance( "format_credit_card_cvc" );
                vm.virtuals.CARD_NAME = res.data.FORENAME + " " + res.data.SURNAME;
                LoginService.setCustomer( res.data );
            } )
        vm.updateDetails = function() {
            if ( parsley.validate() ) {
                if ( vm.updatePassword ) vm.data.PASSWORD_HASH = vm.newPassword;
                LoginService.updateCustomer( vm.data.ID, vm.data )
                    .then( function() {
                        toastr.success( 'Details updated' );
                        $location.path( '/' );
                    } )
            }
        }
    } ] )
    .controller( 'MyBookings', [ 'LoginService', 'BookingService', 'EventsService', '$location', function( LoginService, BookingService, EventsService, $location ) {
        var vm = this;
        if ( !LoginService.isLoggedIn() ) $location.url( '/login?return_to=/bookings' );
        else BookingService.getCustomerBookings( LoginService.getCustomer()
                .ID )
            .then( function( res ) {
                vm.bookings = res.data;
                vm.bookings.forEach( function( item ) {
                    EventsService.get( item.RUN.EVENT_ID )
                        .then( function( res ) {
                            item.EVENT = res.data;
                        } )
                } )
            } )
    } ] )
    .controller( 'HomePageController', [ 'EventsService', function( EventsService ) {
        var vm = this;
        EventsService.getAll()
            .then( function( res ) {
                vm.list = res.data.map( sortRuns )
                    .map( avgReviews );
            } )
    } ] )
    .controller( 'EventsListController', [ 'EventsService', function( EventsService ) {
        var vm = this;
        vm.page = 1;
        vm.perPage = 5;
        EventsService.getTypes()
            .then( function( res ) {
                vm.eventTypes = res.data;
            } )
        EventsService.getAll()
            .then( function( res ) {
                vm.list = res.data.map( sortRuns )
                    .map( avgReviews );
            } )
    } ] )
    .controller( 'LoginForm', [ 'LoginService', '$location', '$routeParams', function( LoginService, $location, $routeParams ) {
        var vm = this;
        var parsley = $( '#login-form' )
            .parsley();
        vm.login = {
            "error": false
        };
        $( '#login-form' )
            .find( 'input' )
            .keypress( function( e ) {
                // Enter pressed?
                if ( e.which == 10 || e.which == 13 ) {
                    vm.loginSubmit();
                }
            } );
        vm.loginSubmit = function() {
            if ( parsley.validate() ) {
                vm.login.error = false;
                LoginService.customerLoginCheck( vm.login.email, vm.login.password )
                    .then( function( res ) {
                        LoginService.setCustomer( res.data );
                        toastr.success( 'Login successful!' );
                        $location.url( $routeParams.return_to || '/' );
                    }, function( res ) {
                        vm.login.error = true;
                    } );
            }
        }
    } ] )
    .controller( 'RegisterForm', [ 'LoginService', '$location', function( LoginService, $location ) {
        var vm = this;
        var parsley = $( '#register-form' )
            .parsley();
        $( '#card-num' )
            .formance( "format_credit_card_number" );
        $( '#card-cvc' )
            .formance( "format_credit_card_cvc" );
        vm.card_name = function() {
            if ( typeof vm.data == "undefined" || typeof vm.data.FORENAME == "undefined" || typeof vm.data.SURNAME == "undefined" ) return "";
            return vm.data.FORENAME + " " + vm.data.SURNAME;
        }
        vm.registerSubmit = function() {
            if ( parsley.validate() ) {
                LoginService.createCustomer( vm.data )
                    .then( function( res ) {
                        LoginService.setCustomer( res.data );
                        toastr.success( 'Registration successful!' );
                        $location.path( '/' );
                    } );
            }
        }
    } ] )
    .controller( 'EventDetailForm', [ 'EventsService', '$routeParams', 'LoginService', 'ReviewsService', function( EventsService, $routeParams, LoginService, ReviewsService ) {
        var vm = this;
        vm.id = $routeParams.id;
        vm.review = {
            USER_RATING: 0
        }
        EventsService.get( $routeParams.id )
            .then( function( res ) {
                vm.event = sortReviews( avgReviews( sortRuns( res.data ) ) );
            } );
        vm.isLoggedIn = function() {
            return LoginService.isLoggedIn();
        }
        vm.submitReview = function() {
            vm.review.USER_ID = LoginService.getCustomer()
                .ID;
            vm.review.EVENT_ID = $routeParams.id;
            ReviewsService.create( vm.review )
                .then( function( res ) {
                    EventsService.get( $routeParams.id )
                        .then( function( res ) {
                            vm.event = sortReviews( avgReviews( sortRuns( res.data ) ) );
                        } )
                } );
        }
    } ] )
    .controller( 'TicketSelectForm', [ 'EventsService', 'RunsService', 'PricesService', '$routeParams', 'LoginService', '$location', function( EventsService, RunsService, PricesService, $routeParams, LoginService, $location ) {
        var vm = this;
        var loginForm = $( '#login-form' )
            .parsley();
        vm.event_id = $routeParams.event_id;
        vm.run_id = $routeParams.run_id;
        vm.price_id = $routeParams.price_id;
        vm.isLoggedIn = function() {
            return LoginService.isLoggedIn();
        }
        vm.customer = LoginService.getCustomer();
        vm.login = {
            error: false
        }
        vm.quantity_error = false;
        EventsService.get( vm.event_id )
            .then( function( res ) {
                vm.event = res.data;
            } )
        RunsService.get( vm.run_id )
            .then( function( res ) {
                vm.run = res.data;
            } )
        PricesService.get( vm.price_id )
            .then( function( res ) {
                vm.price = res.data;
            } )
        $( '#login-form' )
            .find( 'input' )
            .keypress( function( e ) {
                // Enter pressed?
                if ( e.which == 10 || e.which == 13 ) {
                    vm.loginCheckout();
                }
            } );
        vm.loginCheckout = function() {
            if ( vm.quantity > 0 && loginForm.validate() ) {
                vm.login.error = false;
                LoginService.customerLoginCheck( vm.login.email, vm.login.password )
                    .then( function( res ) {
                        LoginService.setCustomer( res.data );
                        $location.path( $location.path() + '/checkout/' + vm.quantity );
                        //successful login
                    }, function( res ) {
                        vm.login.error = true;
                    } );
            } else if ( vm.quantity == 0 | typeof vm.quantity == "undefined" ) {
                vm.quantity_error = true;
                $( 'html, body' )
                    .animate( {
                        scrollTop: 0
                    }, 800 );
            }
        }
        vm.checkout = function() {
            if ( vm.quantity > 0 ) $location.path( $location.path() + '/checkout/' + vm.quantity );
            else {
                vm.quantity_error = true;
                $( 'html, body' )
                    .animate( {
                        scrollTop: 0
                    }, 800 );
            }
        }
        vm.guestCheckout = function() {
            if ( vm.quantity > 0 ) $location.path( $location.path() + '/guest_checkout/' + vm.quantity );
            else {
                vm.quantity_error = true;
                $( 'html, body' )
                    .animate( {
                        scrollTop: 0
                    }, 800 );
            }
        }
    } ] )
    .controller( 'CheckoutForm', [ 'EventsService', 'RunsService', 'PricesService', 'BookingService', '$routeParams', 'LoginService', '$location', function( EventsService, RunsService, PricesService, BookingService, $routeParams, LoginService, $location ) {
        var vm = this;
        vm.savedPaymentDetails = true;
        vm.stage = 0;
        vm.data = {};
        vm.data.personID = LoginService.customer.ID;
        vm.data.quantity = $routeParams.quantity;
        vm.data.runID = $routeParams.run_id;
        $( '#card-num' )
            .formance( "format_credit_card_number" );
        $( '#card-cvc' )
            .formance( "format_credit_card_cvc" );
        EventsService.get( $routeParams.event_id )
            .then( function( res ) {
                vm.event = res.data;
            } )
        RunsService.get( $routeParams.run_id )
            .then( function( res ) {
                vm.run = res.data;
            } )
        PricesService.get( $routeParams.price_id )
            .then( function( res ) {
                vm.price = res.data;
                vm.data.runID = res.data.RUN_ID;
                vm.data.tierID = res.data.TIER_ID;
                vm.data.totalCost = res.data.PRICE1 * vm.data.quantity;
            } )
        vm.submit = function() {
            BookingService.createBooking( vm.data )
                .then( function( res ) {
                    vm.stage = 1;
                }, function( res ) {
                    vm.stage = 2;
                } )
        }
    } ] )
    .controller( 'GuestCheckoutForm', [ 'EventsService', 'RunsService', 'PricesService', 'BookingService', '$routeParams', '$location', function( EventsService, RunsService, PricesService, BookingService, $routeParams, $location ) {
        var vm = this;
        vm.stage = 0;
        vm.data = {};
        vm.data.quantity = $routeParams.quantity;
        $( '#card-num' )
            .formance( "format_credit_card_number" );
        $( '#card-cvc' )
            .formance( "format_credit_card_cvc" );
        EventsService.get( $routeParams.event_id )
            .then( function( res ) {
                vm.event = res.data;
            } )
        RunsService.get( $routeParams.run_id )
            .then( function( res ) {
                vm.run = res.data;
            } )
        PricesService.get( $routeParams.price_id )
            .then( function( res ) {
                vm.price = res.data;
                vm.data.runID = res.data.RUN_ID;
                vm.data.tierID = res.data.TIER_ID;
                vm.data.totalCost = res.data.PRICE1 * vm.data.quantity;
            } )
        vm.submit = function() {
            BookingService.createGuestBooking( vm.data )
                .then( function( res ) {
                    vm.stage = 1;
                }, function( res ) {
                    vm.stage = 2;
                } )
        }
    } ] );